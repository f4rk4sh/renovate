FROM python:3.12.2-slim-bullseye

WORKDIR /src
COPY . /src

RUN apt-get update -y \
    && apt-get install -y --no-install-recommends --no-upgrade \
    libsnappy-dev=1.1.8-1 \
    git-lfs=2.13.2-1+b5 \
    git=2.30.2-1 \
    curl \
    && python -m pip install --no-cache-dir virtualenv==20.27.1 \
    && curl -sSL https://install.python-poetry.org | POETRY_VERSION=1.8.4 python3 - && poetry --version \
    && apt-get purge --auto-remove -y build-essential curl \
    && apt-get -y autoremove && apt-get clean && rm -rf /var/lib/apt/lists/*
