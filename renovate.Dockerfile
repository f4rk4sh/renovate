# syntax=docker/dockerfile:1.4
FROM renovate/renovate:37.440-slim
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
# We need to have all Python versions used by our projects.
USER root
RUN install-tool python 3.12.2 \
    && install-tool poetry 1.8.4
